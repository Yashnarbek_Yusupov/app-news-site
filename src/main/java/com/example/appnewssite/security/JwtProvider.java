package com.example.appnewssite.security;

import com.example.appnewssite.entity.Lavozim;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import javax.management.relation.Role;
import java.util.Date;
import java.util.Set;

@Component
public class JwtProvider {
    private static final long exporeTime = 1000*60*60*24; //millisekundda

    private static final String secretKey = "MaxfiySozKamida256Bit(32Belgi)liBolishiKerak";

    public String generateToken(String username, Lavozim lavozim){
        Date expireDate = new Date(System.currentTimeMillis() + exporeTime);

        String token  = Jwts
                .builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
                .claim("lavozims", lavozim.getName())
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
        return token;
    }

    public String getUsernameFromToken(String token){
        try{
            String username = Jwts
                    .parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
            return username;
        }
        catch(Exception e){
            return null;
        }
    }


}

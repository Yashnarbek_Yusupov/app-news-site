package com.example.appnewssite.aop;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)  //Qayerda ishlatmoqchimiz, methodda, o'zgaruvchida, constructorda ... va h.zo
@Retention(RetentionPolicy.RUNTIME) // qachon ishga tushsin, dastur ishga tushganda
public @interface CheckPermission {
    String huquq();
}

package com.example.appnewssite.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException{
    private String resourceName;
    private String resourceField;
    private Object object;

//    public ResourceNotFoundException(String resourceName, String resourceField) {
//        this.resourceName = resourceName;
//        this.resourceField = resourceField;
//    }
}

package com.example.appnewssite.service;

import com.example.appnewssite.dto.ApiResponse;
import com.example.appnewssite.dto.LoginDto;
import com.example.appnewssite.dto.RegisterDto;
import com.example.appnewssite.entity.Lavozim;
import com.example.appnewssite.entity.User;
import com.example.appnewssite.exceptions.ResourceNotFoundException;
import com.example.appnewssite.repository.LavozimRepository;
import com.example.appnewssite.repository.UserRepository;
import com.example.appnewssite.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    LavozimRepository lavozimRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    public ApiResponse registerUser(RegisterDto registerDto) {
            if(!registerDto.getPassword().equals(registerDto.getPrepassword())){
                return new ApiResponse("Parollar mos emas", false);
            }

        boolean existsByUsername = userRepository.existsByUsername(registerDto.getUsername());
        if(existsByUsername)
            return new ApiResponse("Bunday username avvaldan ro'yxatdan o'tgan", false);
        User user = new User(
                registerDto.getFullname(),
                registerDto.getUsername(),
                passwordEncoder.encode(registerDto.getPassword()),
                lavozimRepository.findByName(AppConstants.USER).orElseThrow(() -> new ResourceNotFoundException("Lavozim", "name", AppConstants.USER)),
                true
        );
        userRepository.save(user);
        return new ApiResponse("Muvaffaqiyatli ro'yxatdan o'tdingiz", true);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }
}

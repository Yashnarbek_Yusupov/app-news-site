package com.example.appnewssite.service;

import com.example.appnewssite.dto.ApiResponse;
import com.example.appnewssite.dto.LavozimDto;
import com.example.appnewssite.entity.Lavozim;
import com.example.appnewssite.repository.LavozimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LavozimService {
    @Autowired
    LavozimRepository lavozimRepository;
    public ApiResponse addLavozim(LavozimDto lavozimDto) {
        if(lavozimRepository.existsByName(lavozimDto.getName()))
            return new ApiResponse("Bunday nomdagi lavozim oldindan mavjud", false);
        Lavozim lavozim = new Lavozim();
        lavozim.setName(lavozimDto.getName());
        lavozim.setDescription(lavozimDto.getDescription());
        lavozim.setHuquqList(lavozimDto.getHuquqList());
        lavozimRepository.save(lavozim);
        return new ApiResponse("Lavozim muvaffaqiyatli qo'shildi", true);
    }

    public ApiResponse editLavozim(Long id, LavozimDto lavozimDto) {

        return new ApiResponse("Lavozim muvaffaqiyatli o'zgartirildi", true);
    }
}

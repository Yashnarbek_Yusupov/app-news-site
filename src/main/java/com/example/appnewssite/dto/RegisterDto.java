package com.example.appnewssite.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    @NotNull(message = "Fullname bo'sh bo'lmasligi kerak")
    private String fullname;

    @NotNull(message = "Username bo'sh bo'lmasligi kerak")
    private String username;

    @NotNull(message = "Parol bo'sh bo'lmasligi kerak")
    private String password;

    @NotNull(message = "Takroriy parol bo'sh bo'lmasligi kerak")
    private String prepassword;

}

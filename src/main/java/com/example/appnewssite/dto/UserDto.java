package com.example.appnewssite.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @NotNull(message = "Fullname bo'sh bo'lmasligi kerak")
    private String fullname;

    @NotNull(message = "Username bo'sh bo'lmasligi kerak")
    private String username;

    @NotNull(message = "Parol bo'sh bo'lmasligi kerak")
    private String password;

    @NotNull(message = "Lavozim bo'sh bo'lmasligi kerak")
    private Integer lavoizmId;

}

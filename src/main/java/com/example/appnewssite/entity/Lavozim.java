package com.example.appnewssite.entity;

import com.example.appnewssite.entity.enums.Huquq;
import com.example.appnewssite.entity.template.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Lavozim extends AbstractEntity {

    @Column(unique = true, nullable = false)
    private String name; // ADMIN, USER, BOSHQALAR

    @Enumerated(value = EnumType.STRING)
    @ElementCollection(fetch = FetchType.LAZY)
    private List<Huquq> huquqList;

    @Column(columnDefinition = "text")
    private String description;


    public Lavozim(String name, List<Huquq> huquqList) {
        this.name = name;
        this.huquqList = huquqList;
    }
}

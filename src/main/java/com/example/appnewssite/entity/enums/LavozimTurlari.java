package com.example.appnewssite.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public enum LavozimTurlari {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_CUSTOM
}

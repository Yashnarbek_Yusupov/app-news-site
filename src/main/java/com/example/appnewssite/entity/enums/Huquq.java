package com.example.appnewssite.entity.enums;

public enum Huquq {
    ADD_USER, //ADMIN
    EDIT_USER, //ADMIN
    DELETE_USER, //ADMIN
    VIEW_USERS, //ADMIN
    ADD_LAVOZIM, //ADMIN
    EDIT_LAVOZIM, //ADMIN
    DELETE_LAVOZIM, //ADMIN
    VIEW_LAVOZIMLAR, //ADMIN
    ADD_POST,  //ADMIN,....
    EDIT_POST, //ADMIN,....
    DELETE_POST, //ADMIN,....
    ADD_COMMENT, //ALL
    EDIT_COMMENT, //ALL
    DELET_MY_COMMENT, //ALL
    DELETE_COMMENT //ADMIN,....
}

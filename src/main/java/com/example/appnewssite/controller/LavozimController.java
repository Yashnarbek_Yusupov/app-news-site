package com.example.appnewssite.controller;


import com.example.appnewssite.aop.CheckPermission;
import com.example.appnewssite.dto.ApiResponse;
import com.example.appnewssite.dto.LavozimDto;
import com.example.appnewssite.service.LavozimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/lavozim")
public class LavozimController {
    @Autowired
    LavozimService lavozimservice;


    @PreAuthorize(value = "hasAuthority('ADD_LAVOZIM')")
    @PostMapping
    public ResponseEntity<?> addLavozim(@Valid @RequestBody LavozimDto lavozimDto){
        ApiResponse apiResponse = lavozimservice.addLavozim(lavozimDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @CheckPermission(huquq = "EDIT_LAVOZIM")
    @PutMapping("/{id}")
    public ResponseEntity<?> editLavozim(@PathVariable Long id, @Valid @RequestBody LavozimDto lavozimDto){
        ApiResponse apiResponse = lavozimservice.editLavozim(id,lavozimDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

}
